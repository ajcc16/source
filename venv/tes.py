# coding=utf-8
import base64
import execjs
import json
import platform
import requests
import sys
import time


def download_videofile(video_links):
    savefile = ""
    if platform.system() == 'Windows':

        savefile = 'F:/APK/mgsInfo/BaiduNetdiskWorkspace/DouYin/videos/'
        # print('Windows')
    elif platform.system() == 'Linux':
        savefile = './videos/'
        # print('Linux')
    else:
        # print('Others')
        pass

    counts = 0
    for link in video_links:
        file_name = link['desc']
        file_url = link['video_url']
        counts += 1
        print("一共%d个文件，正在下载%d个，文件名:%s ..." % (len(video_links), counts, file_name))
        r = requests.get(file_url)
        with open(savefile + file_name + ".mp4", "wb") as code:
            code.write(r.content)
        print("%s 下载完成!\n" % file_name)
    print("所有视频下载完成!")
    return


def dy_sign(method, kw=None):
    # 获取js脚本路径
    signature = getSignaturePath()
    # 执行js获取请求抖音url
    js, reqUrl, headers = getDYUrl(kw, method, signature)
    # 请求url获取json数据
    repJson = requests.get(reqUrl, headers=headers)
    aca = repJson.text
    # 解析数据，保存到字典中author
    author = {}
    savePerList = []
    # 解析首次视频列表然后获取到max_cursor，后续翻页根据max_cursor进行翻页
    author['nickname'] = repJson.json()['aweme_list'][0]['author']['nickname']
    author['short_id'] = repJson.json()['aweme_list'][0]['author']['short_id']
    author['sec_uid'] = repJson.json()['aweme_list'][0]['author']['sec_uid']
    author['avatar'] = getAvatar(repJson)

    max_cursor = parseRepJson(repJson, savePerList)
    # 解析根据max_cursor获取翻页的视频列表
    max_cursor_list = []
    while max_cursor is not None:
        max_cursor_list.append(max_cursor)
        repJson = getDYJsonByMax_cursor(headers, js, kw, max_cursor)
        max_cursor = parseRepJson(repJson, savePerList)

    author['max_cursor'] = max_cursor_list[len(max_cursor_list) - 2]
    author['per_list'] = savePerList
    jj = json.dumps(author, ensure_ascii=False)
    print(jj.encode('UTF-8'))


def dy_sign_now(method, kw=None, max_cursor=None):
    # 获取js脚本路径
    signature = getSignaturePath()
    # 执行js获取请求抖音url
    js, reqUrl, headers = getDYUrl(kw, method, signature)
    # 请求url获取json数据

    # 解析数据，保存到字典中author
    author = {}
    savePerList = []
    authorOff = 0
    max_cursor_list = []
    # 解析根据max_cursor获取翻页的视频列表
    while max_cursor is not None:
        max_cursor_list.append(max_cursor)
        repJson = getDYJsonByMax_cursor(headers, js, kw, max_cursor)
        # acac = repJson.text
        max_cursor = parseRepJson(repJson, savePerList)
        if authorOff == 0 and max_cursor is not None:
            authorOff = 1
            # 设置作者对象
            author['nickname'] = repJson.json()['aweme_list'][0]['author']['nickname']
            author['short_id'] = repJson.json()['aweme_list'][0]['author']['short_id']
            author['sec_uid'] = repJson.json()['aweme_list'][0]['author']['sec_uid']

            author['avatar'] = getAvatar(repJson)
    author['max_cursor'] = max_cursor_list[len(max_cursor_list) - 2]
    author['per_list'] = savePerList
    jj = json.dumps(author, ensure_ascii=False)
    print(jj.encode('utf8'))


def getAvatar(repJson):
    return repJson.json()['aweme_list'][0]['author']['avatar_300x300']['url_list']


def getDYJsonByMax_cursor(headers, js, kw, max_cursor):
    """
根据max_cursor请求抖音
    :param headers:
    :param js:
    :param kw:
    :param max_cursor:
    :return:
    """

    # 根据max_cursor获取翻页后视频json数据
    return requests.get(js.call("aweme_post01", kw, max_cursor), headers=headers)


def parseRepJson(repJson, savePerList):
    """
解析抖音返回数据
    :param repJson:
    :param savePerList:
    :return:
    """
    max_cursor = repJson.json()['max_cursor']
    if max_cursor is None:
        return max_cursor
    aweme_list = repJson.json()['aweme_list']
    # 解析视频列表
    if aweme_list is None:
        return None
    parseAwemeList(aweme_list, savePerList)
    return max_cursor


def getDYUrl(kw, method, signature):
    with open(signature, 'r', encoding='utf-8') as f:
        b = f.read()
    js = execjs.compile(b)
    reqUrl = js.call(method, kw)
    headers = {
        "Accept": "application/json, text/plain, */*",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36",
        "Referer": "https://www.douyin.com/",
        "Accept-Language": "zh-CN,zh;q=0.9",
        "cookie": "ttwid=1%7C58LrEh-Fo8dQ3HX9zJHsYaub_KvGdnjjOXjj_eQqDmE%7C1629706582%7C60c57d8d5e96b93ad8ddb1df9fe073d00b28b0ed7bf15406e547d67227f2039a; MONITOR_WEB_ID=1ff25a6f-c312-4357-844d-4db725f17968; passport_csrf_token_default=f8ac17ec1ed956d733b93f6b893528d4; passport_csrf_token=f8ac17ec1ed956d733b93f6b893528d4; ttcid=0660f6d65c044f09ab3a41aeee66c8f627; s_v_web_id=verify_65a314c4f9998290a0e5209e469dff67; odin_tt=60e63c9f0ef567fda0d13af78920f8f5bdbbd3db4fda6dd136674f112afc73358aae6f03b55adcf40590c62855457f96491186b1f17ba2f621124635ae8ebd9d; n_mh=LGESIkrIW6wsHJHQaRy8vq__EpqWigcb-ex1MnhyToA; sso_auth_status=0ccede5bd49c5408111e1036737c05ec; sso_auth_status_ss=0ccede5bd49c5408111e1036737c05ec; sso_uid_tt=988eb87bfb73da0f499b6c4b6a24265b; sso_uid_tt_ss=988eb87bfb73da0f499b6c4b6a24265b; toutiao_sso_user=d3d6e29ef4e115723d561f5b99099b0c; toutiao_sso_user_ss=d3d6e29ef4e115723d561f5b99099b0c; d_ticket=6263f63bd7191779ffd475133abe3a38207e4; passport_auth_status_ss=bb8066515c641835ea45b7dca9493a11%2Cbae8cb3803e8bf8f5b2628773af458b3; sid_guard=654ca62e8488ec23fcb5e5aa3b3e557d%7C1630375262%7C5184000%7CSat%2C+30-Oct-2021+02%3A01%3A02+GMT; uid_tt=0f8ff41fdd7d3366fed146a05d344c1c; uid_tt_ss=0f8ff41fdd7d3366fed146a05d344c1c; sid_tt=654ca62e8488ec23fcb5e5aa3b3e557d; sessionid=654ca62e8488ec23fcb5e5aa3b3e557d; sessionid_ss=654ca62e8488ec23fcb5e5aa3b3e557d; sid_ucp_v1=1.0.0-KDAyMzk1OTM1ZmE1MjYzOTg3OWY1NjFmYzhiM2UwZmJhYjg5YjAzODQKFgjD6vCL9IwYEN6atokGGO8xOAJA8QcaAmxmIiA2NTRjYTYyZTg0ODhlYzIzZmNiNWU1YWEzYjNlNTU3ZA; ssid_ucp_v1=1.0.0-KDAyMzk1OTM1ZmE1MjYzOTg3OWY1NjFmYzhiM2UwZmJhYjg5YjAzODQKFgjD6vCL9IwYEN6atokGGO8xOAJA8QcaAmxmIiA2NTRjYTYyZTg0ODhlYzIzZmNiNWU1YWEzYjNlNTU3ZA; passport_auth_status=bb8066515c641835ea45b7dca9493a11%2Cbae8cb3803e8bf8f5b2628773af458b3; tt_scid=jifvx9N-P2MLwBpabXUnwJhlZxTOLEOfq5NsC9ZLU8r5KPwi14DluMNt5friG8T40fd4; msToken=Iis3JZwOEJHR0X1hyIoEPsuGs8v7lCmfOZcGqdK36Itn9q20uik6HouYmOslaKSadUgv5eJ3ZB-Z_IkXAqcZH_KgA_P092lmqfGBo_UTcTqE5AInAQzptEg="

    }
    return js, reqUrl, headers


def getSignaturePath():
    if platform.system() == 'Windows':
        return 'F:/BaiduNetdiskWorkspace/DouYin/source/config/signature.js'
        # print('Windows')
    elif platform.system() == 'Linux':
        return '/root/java/demo/dy/pyScirpt/signature.js'
        # print('Linux')
    else:
        #  print('Others')
        pass
    return ''


def parseAwemeList(aweme_list, savePerList):
    """获取视频列表中信息，保存到字典中

    :param ac:视频列表
    """
    for i in aweme_list:
        per = {}

        per['description'] = i['desc'].replace("\"", "").replace("\”", "").replace(" ", "").replace("%", "")
        per['aweme_id'] = i['aweme_id']
        per['video_url'] = i['video']['play_addr']['url_list'][2]
        per['nickname'] = i['author']['nickname']
        per['short_id'] = i['author']['short_id']
        per['create_time'] = i['create_time']
        getVideoCover(i, per)

        savePerList.append(per)


def getVideoCover(repJson, saveJson):
    saveJson['video_cover'] = repJson['video']['cover']['url_list']



if __name__ == '__main__':
    print(dy_sign(method='aweme_post',kw='MS4wLjABAAAAoFUS9aYgIaBOviJtOUzXsG5l_7qa8FWzOmQTxOyrU-w'))

    awemeid = sys.argv[1]
    cmdType = sys.argv[2]
    # param = sys.argv[3]

    # print(sys.argv[1])
    # print(666)
    # with open(FILEPATH, 'r',encoding='utf-8') as load_f:
    #     load_dict = json.load(load_f)
    #
    # print(load_dict)
    # sr_dict= json.loads(load_dict)
    # download_videofile(sr_dict)

    # 首页推荐视频
    # print(dy_sign(method='feed'))
    # 搜索视频
    # print(dy_sign(method='search_item',kw='Lx'))
    # 评论
    # print(dy_sign(method='cooment',kw='6989198974582263070'))
    # 作品
    try:
        if cmdType == 'VIDEO_LIST':
            dy_sign(method='aweme_post', kw=awemeid)
        elif cmdType == 'NEW_VIDEO':
            # 该功能暂不支持
            # dy_sign_now(method='aweme_post', kw=awemeid, max_cursor=param)
            pass
    except Exception as e:
        print("===========", e)

    # print(dy_sign(method='aweme_post',kw='MS4wLjABAAAAoFUS9aYgIaBOviJtOUzXsG5l_7qa8FWzOmQTxOyrU-w'))

    # print(dy_sign(method='aweme_post',kw='MS4wLjABAAAAIWFmTfNJmRajbViR_rK6iGgQMIq0lAWdFmQ5z6iU9Vd4uo9KXOgcJE0o5Dn0JAmW'))
    # TODO 其他的自行补充吧



